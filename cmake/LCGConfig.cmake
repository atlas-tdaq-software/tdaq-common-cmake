#
# File implementing the code that gets called when a project imports
# LCG using something like:
#
#  find_package( LCG 97 )
#
# The script takes the value of the environment variable (or CMake cache
# variable) LCG_RELEASES_BASE to find LCG releases. Or if it's not set, it
# looks up the releases from AFS.
#

# If LCG was already found, don't execute this code again:
if( LCG_FOUND )
   return()
endif()

# This function is used by the code to get the "compiler portion"
# of the platform name. E.g. for GCC 4.9.2, return "gcc49". In case
# the compiler and version are not understood, the functions returns
# a false value in its second argument.
#
# The decision is made based on the C++ compiler.
#
# Usage: lcg_compiler_id( _cmp _isValid )
#
function( lcg_compiler_id compiler isValid )

   # Translate the compiler ID:
   set( _prefix )
   if( CMAKE_CXX_COMPILER_ID STREQUAL "GNU" )
      set( _prefix "gcc" )
   elseif( CMAKE_CXX_COMPILER_ID STREQUAL "Clang" )
      set( _prefix "clang" )
   elseif( CMAKE_CXX_COMPILER_ID STREQUAL "Intel" )
      set( _prefix "icc" )
   else()
      set( ${compiler} "unknown" PARENT_SCOPE )
      set( ${isValid} FALSE PARENT_SCOPE )
      return()
   endif()

   # Translate the compiler version:
   set( _version )
   if( CMAKE_CXX_COMPILER_VERSION MATCHES "^([0-9]+).*" )
      set( _version "${CMAKE_MATCH_1}" )
   endif()

   # Handle new GCC 7 case
   if(${_prefix} STREQUAL "gcc" AND ${_version} MATCHES "[789].*")
     string(SUBSTRING ${_version} 0 1 _version)
   endif()

   # Set the return variables:
   set( ${compiler} "${_prefix}${_version}" PARENT_SCOPE )
   set( ${isValid} TRUE PARENT_SCOPE )

endfunction( lcg_compiler_id )

# This function is used to get a compact OS designation for the platform
# name. Like "slc6", "mac1010" or "cc7".
#
# Usage: lcg_os_id( _os _isValid )
#
function( lcg_os_id os isValid )

   if(DEFINED ENV{TDAQ_OS_ID})
      set(TDAQ_OS_ID $ENV{TDAQ_OS_ID})
   elseif(${LCG_VERSION_NUMBER} GREATER 103)
      set(TDAQ_OS_ID "el")
   else()
      set(TDAQ_OS_ID "centos")
   endif()

   # Reset the result variable as a start:
   set( _name )

   if( APPLE )
      # Get the MacOS X version number from the command line:
      execute_process( COMMAND sw_vers -productVersion
         TIMEOUT 2
         OUTPUT_VARIABLE _macVers )
      # Parse the variable, which should be in the form "X.Y.Z", or
      # possibly just "X.Y":
      if( _macVers MATCHES "^([0-9]+).([0-9]+).*" )
         set( _name "mac${CMAKE_MATCH_1}${CMAKE_MATCH_2}" )
      else()
         set( ${os} "unknown" PARENT_SCOPE )
         set( ${isValid} FALSE PARENT_SCOPE )
         return()
      endif()
   elseif( UNIX )
      cmake_host_system_information(RESULT _linuxShort QUERY DISTRIB_ID)
      cmake_host_system_information(RESULT _linuxVers QUERY DISTRIB_VERSION_ID)

      if(_linuxShort MATCHES "rhel" OR _linuxShort MATCHES "centos")
         if(_linuxVers MATCHES "7.*")
           set( _linuxShort "centos")
         else()
           set( _linuxShort ${TDAQ_OS_ID})
         endif()
      elseif(_linuxShort MATCHES "almalinux" OR _linuxShort MATCHES "rocky" OR _linuxShort MATCHES "ol")
         set( _linuxShort ${TDAQ_OS_ID} )
      endif()

      if(_linuxShort STREQUAL "ubuntu")
         string(REPLACE "." "" _linuxVers ${_linuxVers})
      else()
         string(REGEX REPLACE "\\..*" "" _linuxVers ${_linuxVers})
      endif()

      set(_name ${_linuxShort}${_linuxVers})
   else()
      set( ${os} "unknown" PARENT_SCOPE )
      set( ${isValid} FALSE PARENT_SCOPE )
      return()
   endif()

   # Set the return values:
   set( ${os} ${_name} PARENT_SCOPE )
   set( ${isValid} TRUE PARENT_SCOPE )

endfunction( lcg_os_id )

# This function is used internally to construct a platform name for a
# project. Something like: "x86_64-slc6-gcc48-opt".
#
# Usage: lcg_platform_id( _platform )
#
function( lcg_platform_id platform )

   # Get the OS's name:
   lcg_os_id( _os _valid )
   if( NOT _valid )
      set( ${platform} "generic" PARENT_SCOPE )
      return()
   endif()

   # Get the compiler name:
   lcg_compiler_id( _cmp _valid )
   if( NOT _valid )
      set( ${platform} "generic" PARENT_SCOPE )
      return()
   endif()

   # Construct the postfix of the platform name:
   if( CMAKE_BUILD_TYPE STREQUAL "Debug" )
      set( _postfix "dbg" )
   else()
      set( _postfix "opt" )
   endif()

   # Set the platform return value:
   set( ${platform} "${CMAKE_SYSTEM_PROCESSOR}-${_os}-${_cmp}-${_postfix}"
      PARENT_SCOPE )

endfunction( lcg_platform_id )

# Get the platform ID:
lcg_platform_id( LCG_PLATFORM )

if(NOT "${LCG_PLATFORM}" STREQUAL "${BINARY_TAG}")

  # Compare to our own binary tag and complain if user asked for something not
  # compatible with environment that he has set up.
  string(REGEX MATCHALL "[a-zA-Z0-9_]+" _platform  ${LCG_PLATFORM})
  string(REGEX MATCHALL "[a-zA-Z0-9_]+" _tag ${BINARY_TAG})

  list(GET _platform 0 _platform_arch)
  list(GET _platform 1 _platform_os)
  list(GET _platform 2 _platform_cxx)

  list(GET _tag 0 _tag_arch)
  list(GET _tag 1 _tag_os)
  list(GET _tag 2 _tag_cxx)

  if(NOT "${_platform_arch}" STREQUAL "${_tag_arch}")
    if(NOT CMAKE_CROSSCOMPILING)
      message(WARNING "Your configuration: ${BINARY_TAG} is not compatible with your architecture setup: ${LCG_PLATFORM}.The CMake configuration will most likely fail !!!")
    endif()
  endif()

  # Check for same compiler
  if(NOT "${_tag_cxx}" STREQUAL "${_platform_cxx}") 
    message(WARNING "Your configuration: ${BINARY_TAG} is not compatible with your compiler setup: ${LCG_PLATFORM}.The CMake configuration will most likely fail !!!")
  endif()

  # Check for possible OS backward compatibity
  if( "${_platform_os}" MATCHES "centos.*" AND "${_tag_os}" STREQUAL "slc6")
    message(STATUS "Cross-compiling from one ${_platform_os} to ${_tag_os}")
  else()
    if(NOT CMAKE_CROSSCOMPILING)
      message(WARNING "Your configuration: ${BINARY_TAG} is not compatible with your OS setup: ${LCG_PLATFORM}.The CMake configuration will most likely fail !!!")
    endif()
  endif()
endif()

# Tell the user what's happening:
message( STATUS
   "Setting up LCG release \"${LCG_VERSION}\" for platform: ${BINARY_TAG}" )

# Some sanity checks:
if( LCG_FIND_COMPONENTS )
  message( WARNING "Components \"${LCG_FIND_COMPONENTS}\" requested, but "
    "finding LCG components is not supported" )
endif()

get_filename_component(LCG_CONFIG_DIR ${LCG_RELEASE_BASE} DIRECTORY)

# Construct the path to pick up the release from:
if("${LCG_VERSION_NUMBER}" STREQUAL "999")
  set( LCG_RELEASE_DIR ${LCG_RELEASE_BASE}/${LCG_VERSION_POSTFIX} )
    set(LCG_CONFIG_DIR ${LCG_CONFIG_DIR}/nightlies/${LCG_VERSION_POSTFIX})
else()
  set( LCG_RELEASE_DIR ${LCG_RELEASE_BASE}/LCG_${LCG_VERSION} )
  set(LCG_CONFIG_DIR ${LCG_CONFIG_DIR}/releases/LCG_${LCG_VERSION} )  
endif()

# The components to set up:
set( LCG_COMPONENTS externals)

# Start out with the assumption that LCG is now found:
set( LCG_FOUND TRUE )

function(lcg_setup_components)

  
  # Set up the packages provided by LCG using the files specified:
  foreach( _component ${LCG_COMPONENTS} )

    # Construct the file name to load:

    set( _file ${LCG_CONFIG_DIR}/LCG_${_component}_${BINARY_TAG}.txt )
    
    if( NOT EXISTS ${_file} )
      message( SEND_ERROR
        "LCG component \"${_component}\" not available for platform: "
        "${BINARY_TAG}" )
      set( LCG_FOUND FALSE PARENT_SCOPE)
      continue()
    endif()
    
    # Tell the user what's happening:
    message( STATUS "Setting up ${LCG_RELEASE_DIR} using: ${_file}" )
    
    # Read in the contents of the file:
    file( STRINGS ${_file} _fileContents REGEX ".*;.*;.*;.*;.*" )
    
    # Loop over each line of the configuration file:
    foreach( _line ${_fileContents} )

      # The component's name:
      list( GET _line 0 name )
      string( TOUPPER ${name} nameUpper )
      # The component's identifier:
      list( GET _line 1 hash )
      string( STRIP ${hash} hash)
      # The component's version:
      list( GET _line 2 version )
      string(STRIP ${version} version)
      # The component's base directory:
      list( GET _line 3 dir1 )
      string( STRIP ${dir1} dir2 )

      # Handle the case where the directory is an absolute path
      # e.g. for LCG nightlies ?
      if(${dir2} MATCHES "^/.*")
        set(dir3 ${dir2})
      else()
        string( REPLACE "./" "" dir3 ${dir2} )
        set(dir3 ${LCG_RELEASE_DIR}/${dir3})
      endif()
      # The component's dependencies, separated by comma, and suffixed with the hash -<HASH>
      list( GET _line 4 deps )

      string( REPLACE "," ";" _deps ${deps})
      set(deps)
      foreach(_d ${_deps})
        string(FIND ${_d} "-" _idx REVERSE)
        string(SUBSTRING ${_d} 0 ${_idx} _d)
        list(APPEND deps ${_d})
      endforeach()

      if(${name} STREQUAL "CMake")
         continue()
      endif()
      
      # Set up the component. In an extremely simple way for now, which
      # just assumes that the Find<Component>.cmake files will find
      # these components based on the <Component>_ROOT or possibly
      # <Component>_DIR variable.
      if(NOT LCG_USE_VIEW)
        set( ${nameUpper}_ROOT ${dir3}
          CACHE PATH "Directory for ${name}-${version}" FORCE )
        set( ${nameUpper}_DIR ${dir3}
          CACHE PATH "Directory for ${name}-${version}" FORCE )
        set( ${nameUpper}_ROOT_DIR ${dir3}
          CACHE PATH "Directory for ${name}-${version}" FORCE )
      else()
        set( ${nameUpper}_ROOT ${LCG_RELEASE_DIR}/${BINARY_TAG}
          CACHE PATH "Directory for ${name}-${version}" FORCE )
        set( ${nameUpper}_DIR ${LCG_RELEASE_DIR}/${BINARY_TAG}
          CACHE PATH "Directory for ${name}-${version}" FORCE )
        set( ${nameUpper}_ROOT_DIR ${LCG_RELEASE_DIR}
          CACHE PATH "Directory for ${name}-${version}" FORCE )

      endif(NOT LCG_USE_VIEW)

      set( ${nameUpper}_DEPENDENCIES ${deps}
        CACHE PATH "Dependencies for ${name}-${version}" FORCE )
      set( ${nameUpper}_HASH ${hash}
        CACHE STRING "Hash" FORCE )

      set( ${nameUpper}_VERSION ${version} PARENT_SCOPE)
    endforeach()
  endforeach()

endfunction()

lcg_setup_components()

if(NOT LCG_USE_VIEW)

#
# Extra setting(s) for some package(s):
#
file( GLOB BOOST_INCLUDEDIR "${BOOST_ROOT}/include/*" )
set(BOOST_INCLUDEDIR ${BOOST_INCLUDEDIR} CACHE PATH "Boost include directory" FORCE)
file( GLOB BOOST_LIBRARYDIR "${BOOST_ROOT}/lib" )
set(BOOST_LIBRARYDIR ${BOOST_LIBRARYDIR} CACHE PATH "Boost library directory" FORCE)
set(Boost_LIBRARY_DIR ${BOOST_LIBRARYDIR} CACHE PATH "Boost library directory" FORCE)

if(${BINARY_TAG} MATCHES ".*-clang.*")
  string(REPLACE "-" ";" platform ${BINARY_TAG})
  list(GET platform 2 compiler)
  set(Boost_COMPILER "-${compiler}" CACHE STRING "Boost compiler suffix" FORCE)
endif()

# needed for PythonInterp
set(CMAKE_PROGRAM_PATH "${PYTHON_ROOT}/bin")
# needed for PythonLibs
file(GLOB PYTHON_INCLUDE_DIR ${PYTHON_ROOT}/include/python*)
set(PYTHON_INCLUDE_DIR ${PYTHON_INCLUDE_DIR} CACHE PATH "Python include directory" FORCE)
file(GLOB PYTHON_LIBRARY ${PYTHON_ROOT}/lib/libpython?.*.so)
set(PYTHON_LIBRARY ${PYTHON_LIBRARY} CACHE PATH "Python library" FORCE)

# needed for new FindPython.cmake
set(Python_ROOT_DIR ${PYTHON_ROOT} CACHE PATH "Python path" FORCE)

set(pytools_home ${PYTOOLS_ROOT})

# needed ROOT
set(ROOTSYS "${ROOT_ROOT}" CACHE PATH "ROOT directory" FORCE)
set(LibLZMA_ROOT ${XZ_ROOT} CACHE PATH "LZMA directory" FORCE)

# needed for Qt4
set(ENV{QTDIR} ${QT_DIR})
set(QTDIR ${QT_DIR} CACHE PATH "Qt4 directory" FORCE)
set(QT_SEARCH_PATH ${QT_DIR} CACHE PATH "Qt4 directory" FORCE)

# needed for Qt5 to have find_package() work without HINTS ${QT5_ROOT}
set(Qt5_DIR ${QT5ROOT}/lib/cmake/Qt5 CACHE PATH "Qt5 CMake directory" FORCE)

# needed for nlohmann_json
set(NLOHMANN_JSON_ROOT ${JSONMCPP_ROOT} CACHE PATH "nlohmann_json directory" FORCE)

else(NOT LCG_USE_VIEW)

  set(CMAKE_FIND_ROOT_PATH ${LCG_RELEASE_DIR}/${BINARY_TAG} CACHE PATH "LCG Directory")
  
  # set(TBB_ROOT_DIR ${CMAKE_FIND_ROOT_PATH} CACHE PATH "TBB path")
  # set(Python_ROOT_DIR ${CMAKE_FIND_ROOT_PATH} CACHE PATH "Python path")
  # set(ROOTSYS "${CMAKE_FIND_ROOT_PATH}" CACHE PATH "ROOT directory" FORCE)

endif(NOT LCG_USE_VIEW)

# needed for finding the correct version gcc libstdc++
set(gcc_config_version $ENV{gcc_config_version} CACHE STRING "Version of libstd++ to use" FORCE)

# Get the current directory:
get_filename_component( _thisdir "${CMAKE_CURRENT_LIST_FILE}" PATH )

# Add the module directory to CMake's module path:
list( APPEND CMAKE_MODULE_PATH ${_thisdir}/modules )
list( REMOVE_DUPLICATES CMAKE_MODULE_PATH )

list(APPEND TDAQ_RELOCATE_PATHS LCG_RELEASE_BASE ${LCG_RELEASE_BASE})
