
# This is a project dependent include file that is
# used if TDAQ_USE_RUNPATH is true.

# An equivalent file is needed in any down-stream project
# that wants to use the RUNPATH mechanism as well.

set(CMAKE_INSTALL_RPATH "\$ORIGIN/../lib:\$ORIGIN/../../../../../sw/lcg/views/LCG_${LCG_VERSION}/${BINARY_TAG}/lib:$ORIGIN/../../../../../sw/lcg/views/LCG_${LCG_VERSION}/${BINARY_TAG}/lib64")

