## This file should be placed in the root directory of your project.
## Then modify the CMakeLists.txt file in the root directory of your
## project to incorporate the testing dashboard.
##
## # The following are required to submit to the CDash dashboard:
##   ENABLE_TESTING()
##   INCLUDE(CTest)

set(CTEST_PROJECT_NAME "tdaq-common")
set(CTEST_NIGHTLY_START_TIME "00:00:00 CEST")

set(CTEST_SUBMIT_URL "https://atlas-tdaq-cdash.web.cern.ch/submit.php?project=tdaq-common")
set(CTEST_LABELS_FOR_SUBPROJECTS circ cmake_tdaq compression CTPfragment DQConfMaker dqm_algorithm_helper dqm_core eformat ers EventApps EventStorage HistogramStyles hltinterface MuCalDecode TDAQCExternal TDAQCRelease webdaq)

set(CTEST_USE_LAUNCHERS 1)
